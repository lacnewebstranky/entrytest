HOW TO RUN PROGRAM

ensure your environment variable JAVA_HOME is pointing to java 11 jdk
for example: export JAVA_HOME=/usr/lib/jvm/java-11-openjdk/

to run program with file arguments:
mvn spring-boot:run -Dspring-boot.run.arguments="file1,file2,...."
example:mvn spring-boot:run -Dspring-boot.run.arguments="Input,Input2"


run file without arguments:
mvn spring-boot:run

add file on the run:
addfile filename
example: addfile Input1

to exit program:
quit

