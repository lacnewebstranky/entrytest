package bsc.introtest.services.input;

import bsc.introtest.interfaces.input.IInputReader;
import bsc.introtest.services.file.AccountFileLoader;
import bsc.introtest.services.file.AccountLoader;
import bsc.introtest.services.model.AccountModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.NoSuchElementException;
import java.util.Scanner;

@Service
public class ManualInputReader extends AccountLoader implements IInputReader {

    @Autowired
    private AccountModel accountModel;
    @Autowired
    private AccountFileLoader accountFileLoader;
    private Boolean readInput;

    public ManualInputReader() {
        readInput = false;
    }

    @PostConstruct
    private void init() {
        readInput = false;
    }

    /**
     * Starts endless loop of waiting for input until stopped with {@link ManualInputReader#stop()}
     */
    @Override
    public void start() {
        if (readInput) {
            return;
        }
        Scanner scanner = new Scanner(System.in);
        this.readInput = true;
        try {
            while (this.readInput) {
                String line = scanner.nextLine();
                if (line.startsWith("addfile ")) {
                    addTransactionsFilesToAccount(line.replaceFirst("addfile ", "").trim().replaceAll("\\s{2,}", ""));
                } else if (line.startsWith("quit")) {
                    System.exit(0);
                } else {
                    addTransactionToAccount(line.toUpperCase());
                }
            }
        } catch (IllegalStateException | NoSuchElementException e) {
            e.printStackTrace();
            this.readInput = false;
            System.out.println("System.in was closed; exiting");
        }
    }

    private void addTransactionToAccount(String line) {
        line = formatLine(line);
        if (isValidLine(line)) {
            accountModel.updateData(line);
        }
    }

    private void addTransactionsFilesToAccount(String input) {
       accountModel.updateData(accountFileLoader.loadFiles(input.split(" ")));
    }

    /**
     * stops waiting for input
     */
    @Override
    public void stop() {
        this.readInput = false;
    }
}
