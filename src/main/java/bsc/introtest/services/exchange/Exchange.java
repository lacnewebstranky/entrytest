package bsc.introtest.services.exchange;

import bsc.introtest.interfaces.exchange.IExchange;
import bsc.introtest.services.input.ManualInputReader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class Exchange implements IExchange {

    private Map<String, Double> rates;

    @PostConstruct
    private void init() {
        rates = new HashMap<String, Double>();
        rates.put("EUR", 1.2);
        rates.put("AUD", 0.68);
        rates.put("CZK", 0.044);
        rates.put("YEN", 0.0092);
    }

    @Override
    public Double getValueInUSD(String currency, Double value) {
        if (value == null) {
            return null;
        }

        Double rate = this.rates.get(currency.toUpperCase());
        if (rate == null) {
            return null;
        }

        return value * rate;
    }
}
