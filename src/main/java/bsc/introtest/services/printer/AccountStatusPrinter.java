package bsc.introtest.services.printer;

import bsc.introtest.interfaces.exchange.IExchange;
import bsc.introtest.interfaces.printer.IMapPrinter;
import bsc.introtest.services.file.AccountFileLoader;
import bsc.introtest.services.model.AccountModel;
import bsc.introtest.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

@Service
public class AccountStatusPrinter implements IMapPrinter {


    @Autowired
    private AccountModel accountModel;
    @Autowired
    private IExchange exchange;
    private Boolean started;

    @PostConstruct
    private void init() {
        this.started = false;
    }

    @Override
    public IMapPrinter start(Integer frequency) {

        if (!this.started) {
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    Utils.clearConsole();
                    System.out.println("==========================================================");
                    System.out.println("====================== ACCOUNT STATUS ====================");
                    System.out.println("==========================================================");

                    accountModel.getAccount().forEach((key, value) -> {
                        if (value != 0) {
                            Double usdValue = exchange.getValueInUSD(key, value);
                            System.out.format("%s: %.2f" + (usdValue != null ? " ("+usdValue+" USD)" : "")+ "\n", key, value);
                        }
                    });
                }
            }, 0, frequency);

            this.started = true;
        }
        return this;
    }

    @Override
    public IMapPrinter start() {
        // TODO dat default hodnoty do configu
        start(60000);
        return this;
    }


}
