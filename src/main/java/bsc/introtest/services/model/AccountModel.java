package bsc.introtest.services.model;

import bsc.introtest.interfaces.account.IAccount;
import bsc.introtest.interfaces.account.IAccountLoader;
import bsc.introtest.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class AccountModel implements IAccount {

    @Autowired
    @Qualifier("fileLoader")
    private IAccountLoader accountLoader;
    private Map<String, Double> account;

    @PostConstruct
    private void init() {
        this.account = new ConcurrentHashMap<>();
    }

    @Override
    public AccountModel updateData(String... filePaths) {
        this.account = Utils.mergeAccountMaps(this.accountLoader.loadFiles(filePaths), this.account);
        return this;
    }

    @Override
    public AccountModel updateData(ConcurrentHashMap<String, Double> data) {
        this.account = Utils.mergeAccountMaps(data, this.account);
        return this;
    }

    @Override
    public AccountModel updateData(String line) {
        var tempMap = new ConcurrentHashMap<String, Double>();
        var parts = line.split(" ");
        tempMap.put(parts[0], Double.parseDouble(parts[1]));
        this.account = Utils.mergeAccountMaps(account, tempMap);
        return this;
    }

    public Map<String, Double> getAccount() {
        return account;
    }
}
