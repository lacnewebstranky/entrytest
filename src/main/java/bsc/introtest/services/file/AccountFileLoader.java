package bsc.introtest.services.file;

import bsc.introtest.interfaces.account.IAccountLoader;
import bsc.introtest.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 *
 */
@Service
@Qualifier("fileLoader")
public class AccountFileLoader extends AccountLoader implements IAccountLoader {

    /**
     * @param filename
     * @return
     */
    protected ConcurrentHashMap<String, Double> loadFile(String filename) {
        try (Stream<String> stream = Files.lines(Paths.get(filename))) {
            var map = stream
                    .map(line -> formatLine(line))
                    .filter(line -> isValidLine(line))
                    .collect(
                            Collectors.toMap(
                                    s -> s.substring(0, 3),
                                    s -> Double.parseDouble(s.substring(3)),
                                    //if such key exists already we add it together
                                    Double::sum,
                                    ConcurrentHashMap::new
                            )
                    );
            return map;
        } catch (IOException e) {
            System.out.format("File %s does not exist, or you do not have permissions to read it", filename);
        }
        return null;
    }

    @Override
    public ConcurrentHashMap<String, Double> loadFiles(String... filenames) {

        var currencyMaps = new LinkedList<Map<String, Double>>();

        for (String filename : filenames) {
            System.out.println("importing file " + filename);
            var map = loadFile(filename);
            if (map != null) {
                currencyMaps.add(map);
            }
        }

        return Utils.mergeAccountMaps(currencyMaps);
    }
}
