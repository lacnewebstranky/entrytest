package bsc.introtest.services.file;

public abstract class AccountLoader {

    /**
     * @param line
     * Return formatted String for line
     * Removed double and more spaces
     * Removes spaces from begging and ending of text
     * @return
     */
    protected String formatLine(String line) {
        return line.trim().replaceAll("\\s{2,}", " ").replaceAll("- ", "-").toUpperCase();
    }

    // we should consider making this method static if we assume all account loaders will have same input
    protected boolean isValidLine(String line) {

        if (line != null && line.length() > 4) {
            String[] values = line.split(" ");
            if (values.length != 2 || values[0].length() != 3) {
                System.out.println("checking " + line + " - ERROR (INVALID INPUT)");
                return false;
            }
            try {
                Double.parseDouble(values[1]);
            } catch (NumberFormatException e) {
                System.out.println("checking " + line + " - ERROR (NOT A MUNBER)");
                return false;
            } catch (Exception e) {
                System.out.println("checking " + line + " - ERROR (INVALID INPUT)");

                return false;
            }
        } else {
            System.out.println("checking " + line + " - ERROR (INVALID INPUT)");
            return  false;
        }

        System.out.println("checking " + line + " - OK");
        return true;
    }
}
