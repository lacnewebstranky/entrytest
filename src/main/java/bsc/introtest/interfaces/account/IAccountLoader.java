package bsc.introtest.interfaces.account;

import java.util.Map;


/**
 * Loades input files
 */
public interface IAccountLoader {
    Map loadFiles(String... filenames);
}
