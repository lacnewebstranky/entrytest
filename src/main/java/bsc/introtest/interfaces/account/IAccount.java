package bsc.introtest.interfaces.account;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Handles account operations
 */
public interface IAccount {

    IAccount updateData(ConcurrentHashMap<String, Double> data);

    IAccount updateData(String... filePath);

    IAccount updateData(String line);
}
