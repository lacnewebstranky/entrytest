package bsc.introtest.interfaces.printer;

import java.util.concurrent.TimeUnit;

/**
 *
 */
public interface IMapPrinter {

    IMapPrinter start(Integer frequency);

    IMapPrinter start();
}
