package bsc.introtest.interfaces.input;

/**
 *  handels user input
 */
public interface IInputReader {
    void start();
    void stop();
}
