package bsc.introtest.interfaces.exchange;

public interface IExchange {

    Double getValueInUSD(String currency, Double value);
}
