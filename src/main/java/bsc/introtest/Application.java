package bsc.introtest;

import bsc.introtest.interfaces.input.IInputReader;
import bsc.introtest.services.input.ManualInputReader;
import bsc.introtest.services.printer.AccountStatusPrinter;
import bsc.introtest.services.model.AccountModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.concurrent.TimeUnit;

/*
* run from terminal
* mvn spring-boot:run -Dspring-boot.run.arguments="Input,Input2"
* */

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    AccountStatusPrinter accountStatusPrinter;
    @Autowired
    AccountModel accountModel;
    @Autowired
    IInputReader inputReader;

    private static Logger LOG = LoggerFactory
            .getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
        if (args.length > 0) {
            accountModel.updateData(args);
            System.out.println("you can add more files later on the run by:" +
                    " addfile filename");
        } else {
            System.out.println("you started with no file arguments ... you can add it later on the run by:" +
                    " addfile filename");
        }

        accountStatusPrinter.start(20000);
//        accountStatusPrinter.start();
        inputReader.start();
    }
}