package bsc.introtest.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class Utils {

    public static ConcurrentHashMap mergeAccountMaps(List<Map<String, Double>> maps) {
        var result = new ConcurrentHashMap<String, Double>();
        maps.forEach(map -> {
            map.forEach((key, value) -> result.merge(key, value, Double::sum));
        });
        return result;
    }

    public static Map mergeAccountMaps(Map<String, Double>... maps) {
        return mergeAccountMaps(Arrays.asList(maps));
    }

    public static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
            e.printStackTrace();
            // we were unable to clear the console ...
            // new FreddieMercury().sing("nothing really matters")
        }
    }

}
