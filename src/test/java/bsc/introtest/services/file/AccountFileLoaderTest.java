package bsc.introtest.services.file;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

class AccountFileLoaderTest {

    static void writeToFile(String filename, String... input) throws IOException {
        FileWriter fileWriter = new FileWriter(filename);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        for (String line : input) {
            printWriter.println(line);
        }

        printWriter.close();
    }

    @Test
    void loadFile() throws IOException {
        AccountFileLoader accountFileLoader = new AccountFileLoader();
        writeToFile("test", "USD 1", "USD    4", " EUR 5");
        var result = accountFileLoader.loadFile("test");

        assert (result.containsKey("USD"));
        assert (result.containsKey("EUR"));
        assert (result.get("EUR") == 5);
        assert (result.get("USD") == 5);

        new File("test").delete();
    }

    @Test
    void loadFiles() throws IOException {
        AccountFileLoader accountFileLoader = new AccountFileLoader();
        writeToFile("test", "USD -1", "USD    -4", " EUR 5");
        writeToFile("test2", "USD 10", "USD    3", " EUR  - 5");
        var result = accountFileLoader.loadFiles("test", "test2");

        assert (result.containsKey("USD"));
        assert (result.containsKey("EUR"));
        assert (result.get("EUR") == 0);
        assert (result.get("USD") == 8);

        new File("test").delete();
        new File("test2").delete();
    }
}